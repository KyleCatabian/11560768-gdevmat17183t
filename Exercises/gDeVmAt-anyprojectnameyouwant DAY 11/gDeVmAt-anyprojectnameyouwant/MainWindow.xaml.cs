﻿using gDeVmAt_anyprojectnameyouwant.Models;
using gDeVmAt_anyprojectnameyouwant.Utilities;
using SharpGL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;


namespace gDeVmAt_anyprojectnameyouwant
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private Vector3 mousePosition = new Vector3();

        private List<Cube> mgaMassivePlanetsNaHindiNakikita = new List<Cube>();
        private List<Circle> smolSeerkels = new List<Circle>();
        private int time = 0;

        private void OpenGLControl_OpenGLDraw(object sender, SharpGL.SceneGraph.OpenGLEventArgs args)
        {
            this.Title = "GDEVMAT DAY 8";
            OpenGL gl = args.OpenGL;

            // Clear the screen and the depth buffer
            gl.Clear(OpenGL.GL_DEPTH_BUFFER_BIT);
            gl.LoadIdentity();
            gl.Translate(0.0f, 0.0f, -100.0f);

            time++;
            if (time <= 10)
            {
                mgaMassivePlanetsNaHindiNakikita.Add(new Cube()
                {
                    alpha = 0,
                    Mass = RandomNumberGenerator.GenerateInt(5, 10),
                    Position = new Vector3((float)RandomNumberGenerator.GenerateDouble(-30, 30), (float)RandomNumberGenerator.GenerateDouble(-30, 30), 0),
                    G = (float)RandomNumberGenerator.GenerateDouble(0.05f, 0.10f)
                });
                smolSeerkels.Add(new Circle()
                {
                    red = RandomNumberGenerator.GenerateDouble(0, 0.5),
                    green = RandomNumberGenerator.GenerateDouble(0, 0.5),
                    blue = RandomNumberGenerator.GenerateDouble(0, 0.5),
                    alpha = RandomNumberGenerator.GenerateDouble(0.25, 0.75),
                    radius = (float)RandomNumberGenerator.GenerateDouble(1, 3),
                    Position = new Vector3((float)RandomNumberGenerator.GenerateDouble(-75, 75), (float)RandomNumberGenerator.GenerateDouble(-75, 75), 0),
                    Mass = RandomNumberGenerator.GenerateInt(1, 3)
                });
            }

            foreach (var a in mgaMassivePlanetsNaHindiNakikita)
            {
                a.Render(gl);
                foreach (var b in mgaMassivePlanetsNaHindiNakikita)
                {
                    a.ApplyForce(b.CalculateAttraction(a));
                }
                foreach (var c in smolSeerkels)
                {
                    a.ApplyForce(c.CalculateAttraction(a));
                    c.Render(gl);
                    foreach (var d in smolSeerkels)
                    {
                        c.ApplyForce(d.CalculateAttraction(c));
                    }
                    foreach (var e in mgaMassivePlanetsNaHindiNakikita)
                    {
                        c.ApplyForce(e.CalculateAttraction(c));
                    }
                }
            }

            if (time >= 150)
            {
                mgaMassivePlanetsNaHindiNakikita.Clear();
                smolSeerkels.Clear();
                gl.Clear(OpenGL.GL_COLOR_BUFFER_BIT);
                time = 0;
            }
        }

        #region INITIALIZATION
        public MainWindow()
        {
            InitializeComponent();
        }

        private void OpenGLControl_OpenGLInitialized(object sender, SharpGL.SceneGraph.OpenGLEventArgs args)
        {
            OpenGL gl = args.OpenGL;

            gl.Enable(OpenGL.GL_DEPTH_TEST);

            float[] global_ambient = new float[] { 0.5f, 0.5f, 0.5f, 1.0f };
            float[] light0pos = new float[] { 0.0f, 5.0f, 10.0f, 1.0f };
            float[] light0ambient = new float[] { 0.2f, 0.2f, 0.2f, 1.0f };
            float[] light0diffuse = new float[] { 0.3f, 0.3f, 0.3f, 1.0f };
            float[] light0specular = new float[] { 0.8f, 0.8f, 0.8f, 1.0f };

            float[] lmodel_ambient = new float[] { 0.2f, 0.2f, 0.2f, 1.0f };
            gl.LightModel(OpenGL.GL_LIGHT_MODEL_AMBIENT, lmodel_ambient);

            gl.LightModel(OpenGL.GL_LIGHT_MODEL_AMBIENT, global_ambient);
            gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_POSITION, light0pos);
            gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_AMBIENT, light0ambient);
            gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_DIFFUSE, light0diffuse);
            gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_SPECULAR, light0specular);
            gl.Disable(OpenGL.GL_LIGHTING);
            gl.Disable(OpenGL.GL_LIGHT0);

            gl.BlendFunc(OpenGL.GL_SRC_ALPHA, OpenGL.GL_ONE_MINUS_CONSTANT_ALPHA_EXT);
            gl.Enable(OpenGL.GL_BLEND);
            gl.ClearColor(0, 0, 0, 0);

            gl.ShadeModel(OpenGL.GL_SMOOTH);
        }

        private void OpenGLControl_Loaded(object sender, RoutedEventArgs e)
        {

        }
        #endregion

        private void OpenGLControl_MouseMove(object sender, MouseEventArgs e)
        {
            var position = e.GetPosition(this);
            mousePosition.x = (float)position.X - (float)Width / 2.0f;
            mousePosition.y = -((float)position.Y - (float)Height / 2.0f);
        }
    }
}
