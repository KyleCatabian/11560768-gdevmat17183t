﻿using gDeVmAt_anyprojectnameyouwant.Models;
using gDeVmAt_anyprojectnameyouwant.Utilities;
using SharpGL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;


namespace gDeVmAt_anyprojectnameyouwant
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private Vector3 mousePosition = new Vector3();

        private Cube myCube = new Cube();
        private Circle myCircle = new Circle()
        {
            Mass = 20
        };
        private Vector3 wind = new Vector3(0.01f, 0, 0);
        private Vector3 grabeetee = new Vector3(0, -0.2f, 0);
        private Vector3 heeleum = new Vector3(0, 0.1f, 0);
        private int time = 0;

        private void OpenGLControl_OpenGLDraw(object sender, SharpGL.SceneGraph.OpenGLEventArgs args)
        {
            this.Title = "GDEVMAT DAY 7";
            OpenGL gl = args.OpenGL;

            // Clear the screen and the depth buffer
            gl.Clear(OpenGL.GL_COLOR_BUFFER_BIT | OpenGL.GL_DEPTH_BUFFER_BIT);
            gl.LoadIdentity();
            gl.Translate(0.0f, 0.0f, -60.0f);

            myCube.Render(gl);
            myCube.ApplyForce(wind);
            myCube.ApplyForce(grabeetee);
            if (myCube.Position.x >= 45)
            {
                myCube.Velocity.x *= -1;
            }
            if (myCube.Position.x <= -45)
            {
                myCube.Velocity.x *= -1;
            }
            if (myCube.Position.y <= -20)
            {
                myCube.Velocity.y *= -1;
            }
            if (myCube.Position.y >= 20)
            {
                myCube.Velocity.y *= -1;
            }
            myCircle.Render(gl);
            myCircle.ApplyForce(wind);
            myCircle.ApplyForce(heeleum);
            if (myCircle.Position.x >= 45)
            {
                myCircle.Velocity.x *= -1;
            }
            if (myCircle.Position.x <= -45)
            {
                myCircle.Velocity.x *= -1;
            }
            if (myCircle.Position.y <= -20)
            {
                myCircle.Velocity.y *= -1;
            }
            if (myCircle.Position.y >= 20)
            {
                myCircle.Velocity.y *= -1;
            }
            time += 1;
            if (time >= 150)
            {
                myCube.Position = new Vector3(0, 0, 0);
                myCircle.Position = new Vector3(0, 0, 0);
                myCube.Velocity = new Vector3(0, 0, 0);
                myCircle.Velocity = new Vector3(0, 0, 0);
                time = 0;
            }


        }

        #region INITIALIZATION
        public MainWindow()
        {
            InitializeComponent();
        }

        private void OpenGLControl_OpenGLInitialized(object sender, SharpGL.SceneGraph.OpenGLEventArgs args)
        {
            OpenGL gl = args.OpenGL;

            gl.Enable(OpenGL.GL_DEPTH_TEST);

            float[] global_ambient = new float[] { 0.5f, 0.5f, 0.5f, 1.0f };
            float[] light0pos = new float[] { 0.0f, 5.0f, 10.0f, 1.0f };
            float[] light0ambient = new float[] { 0.2f, 0.2f, 0.2f, 1.0f };
            float[] light0diffuse = new float[] { 0.3f, 0.3f, 0.3f, 1.0f };
            float[] light0specular = new float[] { 0.8f, 0.8f, 0.8f, 1.0f };

            float[] lmodel_ambient = new float[] { 0.2f, 0.2f, 0.2f, 1.0f };
            gl.LightModel(OpenGL.GL_LIGHT_MODEL_AMBIENT, lmodel_ambient);

            gl.LightModel(OpenGL.GL_LIGHT_MODEL_AMBIENT, global_ambient);
            gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_POSITION, light0pos);
            gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_AMBIENT, light0ambient);
            gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_DIFFUSE, light0diffuse);
            gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_SPECULAR, light0specular);
            gl.Disable(OpenGL.GL_LIGHTING);
            gl.Disable(OpenGL.GL_LIGHT0);

            gl.BlendFunc(OpenGL.GL_SRC_ALPHA, OpenGL.GL_ONE_MINUS_CONSTANT_ALPHA_EXT);
            gl.Enable(OpenGL.GL_BLEND);
            gl.ClearColor(0, 0, 0, 0);

            gl.ShadeModel(OpenGL.GL_SMOOTH);
        }

        private void OpenGLControl_Loaded(object sender, RoutedEventArgs e)
        {

        }
        #endregion

        private void OpenGLControl_MouseMove(object sender, MouseEventArgs e)
        {
            var position = e.GetPosition(this);
            mousePosition.x = (float)position.X - (float)Width / 2.0f;
            mousePosition.y = -((float)position.Y - (float)Height / 2.0f);
        }
    }
}
