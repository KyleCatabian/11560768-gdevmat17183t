﻿using SharpGL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace gDeVmAt_anyprojectnameyouwant.Models
{
    public abstract class Moveable
    {
        public float posX, posY;
        public double red = 1, green = 1, blue = 1, alpha = 1;

        public abstract void Render(OpenGL gl);
    }
}
