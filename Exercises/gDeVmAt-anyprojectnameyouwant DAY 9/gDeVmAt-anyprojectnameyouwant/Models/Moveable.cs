﻿using SharpGL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace gDeVmAt_anyprojectnameyouwant.Models
{
    public abstract class Moveable
    {
        public Vector3 Position = new Vector3();
        public Vector3 Velocity = new Vector3();
        public Vector3 Acceleration = new Vector3();
        public Vector3 Scale = new Vector3(0.5f, 0.5f, 0.5f);
        public float Mass = 1;

        public double red = 1, green = 1, blue = 1, alpha = 1;

        public void ApplyGravity(float scalar = 0.1f)
        {
            this.Acceleration += (new Vector3(0, -scalar * Mass, 0) / Mass);
        }
        
        public void ApplyForce(Vector3 force)
        {
            // F = MA
            // A = F/M
            this.Acceleration += (force / Mass); // force accumulation
        }

        public abstract void Render(OpenGL gl);
    }
}
