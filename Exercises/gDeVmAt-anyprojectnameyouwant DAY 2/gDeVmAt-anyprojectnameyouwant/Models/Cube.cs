﻿using SharpGL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace gDeVmAt_anyprojectnameyouwant.Models
{
    public class Cube
    {
        public float posX, posY;
        public Color color = new Color();

        public Cube(float x = 0, float y = 0)
        {
            this.posX = x;
            this.posY = y;
        }

        public void Render(OpenGL gl)
        {
            gl.Color(color.red, color.green, color.blue);
            gl.Begin(OpenGL.GL_QUADS);
            // front face
            gl.Vertex(this.posX - 0.5f, this.posY + 0.5f);
            gl.Vertex(this.posX + 0.5f, this.posY + 0.5f);
            gl.Vertex(this.posX + 0.5f, this.posY - 0.5f);
            gl.Vertex(this.posX - 0.5f, this.posY - 0.5f);

            gl.End();
        }

    }
}
