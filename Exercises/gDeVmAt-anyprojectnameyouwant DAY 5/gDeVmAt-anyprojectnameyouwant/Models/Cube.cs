﻿using SharpGL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace gDeVmAt_anyprojectnameyouwant.Models
{
    public class Cube : Moveable
    {
        public Cube(float x = 0, float y = 0, float z = 0)
        {
            this.Position.x = x;
            this.Position.y = y;
            this.Position.z = z;
        }

        public Cube(Vector3 initPos)
        {
            this.Position = initPos;
        }

        public override void Render(OpenGL gl)
        {
            gl.Color(red, green, blue, alpha);
            gl.Begin(OpenGL.GL_QUADS);
            // front face
            gl.Vertex(this.Position.x - 0.5f, this.Position.y + 0.5f);
            gl.Vertex(this.Position.x + 0.5f, this.Position.y + 0.5f);
            gl.Vertex(this.Position.x + 0.5f, this.Position.y - 0.5f);
            gl.Vertex(this.Position.x - 0.5f, this.Position.y - 0.5f);

            gl.End();
        }
    }
}
