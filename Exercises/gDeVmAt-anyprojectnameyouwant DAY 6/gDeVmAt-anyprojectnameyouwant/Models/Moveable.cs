﻿using SharpGL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace gDeVmAt_anyprojectnameyouwant.Models
{
    public abstract class Moveable
    {
        public Vector3 Position = new Vector3();
        public Vector3 Velocity = new Vector3();
        public Vector3 Acceleration = new Vector3();

        public double red = 1, green = 1, blue = 1, alpha = 1;

        public abstract void Render(OpenGL gl);
    }
}
